package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import models.RottenTomatoData;
import models.UserData;
import play.cache.CacheApi;
import play.data.Form;
import play.data.validation.ValidationError;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.confirmview;

import javax.inject.Inject;
import java.util.List;

public class ConfirmController extends Controller {
    @Inject
    WSClient ws;

    @Inject
    CacheApi cache;

    public Result index() {
        Form<UserData> userForm = Form.form(UserData.class).bindFromRequest();
        if (userForm.hasErrors() || userForm.hasGlobalErrors())
            return redirect("/?error=" + getErrors(userForm));
        UserData userData = userForm.get();
        RottenTomatoData rtData = getRT(userData.title);
        if (rtData != null && Math.abs(rtData.rating - userData.rating) <= 0.30000000000001d) {
            cache.set("userHits", cache.getOrElse("userHits", () -> 0) + 1);
            userData.hit = true;
        }

        cache.set("rtData", rtData);
        cache.set("userData", userData);
        return ok(confirmview.render(rtData, userData));
    }

    private String getErrors(Form<UserData> userForm) {
        StringBuilder builder = new StringBuilder();
        for (List<ValidationError> errors : userForm.errors().values()) {
            for (ValidationError error : errors)
                builder.append(error.key()).append(error.key().length() == 0 ? "" : " was ").append(error.message()).append("+");
        }
        return builder.substring(0, builder.length() - 1).replace("error.", "");
    }

    public RottenTomatoData getRT(String q) {
        String url = "http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=9txsnh3qkb5ufnphhqv5tv5z";
        JsonNode json = ws.url(url)
                .setQueryParameter("q", q)
                .setQueryParameter("page_limit", "1")
                .get()
                .map(WSResponse::asJson)
                .get(5000);

        if (json.findPath("total").intValue() <= 0)
            return null;

        ArrayNode jsonArr = (ArrayNode) json.findPath("movies");
        JsonNode jsonNode = jsonArr.get(0);
        String title = jsonNode.findPath("title").textValue();
        int intRating = jsonNode.findPath("ratings").findPath("critics_score").intValue();
        double rating = (double) intRating / 10d;
        String synopsis = jsonNode.findPath("synopsis").textValue();
        return new RottenTomatoData(title, rating, synopsis);
    }
}
