package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.Movie;
import models.MovieComparator;
import models.Statistics;
import play.cache.CacheApi;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.homeview;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HomeController extends Controller {

    @Inject
    CacheApi cache;

    public Result index(String error) {
        Movie newMovie = cache.get("newMovie");
        List<Movie> bottomFive = cache.getOrElse("bottomFive", this::getBottomFive);
        List<Movie> topFive = cache.getOrElse("topFive", this::getTopFive);
        if (newMovie != null) {
            bottomFive = bottomFive.stream().filter(m -> !m.userTitle.equals(newMovie.userTitle)).collect(Collectors.toList());
            bottomFive.add(newMovie);
            bottomFive.sort(MovieComparator.BottomFive);
            bottomFive = bottomFive.subList(0, bottomFive.size() > 5 ? 5 : bottomFive.size());
            topFive = topFive.stream().filter(m -> !m.userTitle.equals(newMovie.userTitle)).collect(Collectors.toList());
            topFive.add(newMovie);
            topFive.sort(MovieComparator.TopFive);
            topFive = topFive.subList(0, topFive.size() > 5 ? 5 : topFive.size());
            cache.set("newMovie", null);
        }
        cache.set("bottomFive", bottomFive);
        cache.set("topFive", topFive);

        Statistics stats = new Statistics(
                cache.getOrElse("userRatingCount", this::getStatistics),
                cache.getOrElse("userRatingTotal", () -> 0d),
                cache.getOrElse("rtRatingCount", () -> 0),
                cache.getOrElse("rtRatingTotal", () -> 0d),
                cache.getOrElse("userHits", () -> 0)
        );
        return ok(homeview.render(error, bottomFive, topFive, stats));
    }

    private int getStatistics() {
        if (Movie.find.findRowCount() == 0)
            return 0;

        String sql = "SELECT SUM(case when(rt_title is null) then 0 else 1 end) as rtRatingCount, SUM(case when(rt_title is null) then 0 else rt_rating end) as rtRatingTotal, COUNT(*) as userRatingCount, SUM(user_rating) as userRatingTotal, SUM(case when (user_hit is true) then 1 else 0 end) as userHits"
                + " from movie";
        RawSql rawSql = RawSqlBuilder.parse(sql).create();
        Statistics stats = Ebean.find(Statistics.class)
                .setRawSql(rawSql)
                .findUnique();

        cache.set("userRatingCount", stats.userRatingCount);
        cache.set("userRatingTotal", stats.userRatingTotal);
        cache.set("rtRatingCount", stats.rtRatingCount);
        cache.set("rtRatingTotal", stats.rtRatingTotal);
        cache.set("userHits", stats.userHits);

        return stats.userRatingCount;
    }

    private List<Movie> getTopFive() {
        List<Movie> movies = Movie.find
                .orderBy("user_rating desc")
                .setMaxRows(5)
                .findList();

        return movies == null
                ? new ArrayList<>()
                : movies;
    }

    private List<Movie> getBottomFive() {
        List<Movie> movies = Movie.find
                .orderBy("user_rating asc")
                .setMaxRows(5)
                .findList();

        return movies == null
                ? new ArrayList<>()
                : movies;
    }
}
