package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.*;
import play.cache.CacheApi;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.movieview;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

public class MovieController extends Controller {

    @Inject
    CacheApi cache;

    public Result index(boolean isTopFive) {
        DynamicForm requestData = Form.form().bindFromRequest();
        String userTitle = requestData.get("userTitle");
        List<Movie> movies = isTopFive
                ? cache.get("topFive")
                : cache.get("bottomFive");

        Movie movie = movies.stream()
                .filter(m -> m.userTitle.equals(userTitle))
                .findFirst()
                .get();

        return ok(movieview.render(movie));
    }

    public Result add() {
        DynamicForm requestData = Form.form().bindFromRequest();
        String newUserRating = requestData.get("newUserRating");
        UserData userData = cache.get("userData");
        RottenTomatoData rtData = cache.get("rtData");
        cache.set("userRatingCount", cache.getOrElse("userRatingCount", () -> 0) + 1);
        cache.set("userRatingTotal", cache.getOrElse("userRatingTotal", () -> 0d) + userData.rating);
        if (rtData != null) {
            cache.set("rtRatingCount", cache.getOrElse("rtRatingCount", () -> 0) + 1);
            cache.set("rtRatingTotal", cache.getOrElse("rtRatingTotal", () -> 0d) + rtData.rating);
        }
        saveOrUpdate(userData, rtData, newUserRating);
        return redirect("/");
    }

    private void saveOrUpdate(UserData userData, RottenTomatoData rtData, String newUserRating) {
        Movie movie = new Movie(userData, rtData, newUserRating);
        Map<String, Long> titleToId = cache.getOrElse("userTitles", this::getTitleToId);
        if (!titleToId.containsKey(movie.userTitle)) {
            titleToId.put(movie.userTitle, getMaxId(titleToId) + 1L);
            movie.save();
        } else {
            Long oldId = titleToId.get(movie.userTitle);
            Movie oldMovie = Movie.find.byId(oldId);
            updateCache(oldMovie);
            movie.id = oldId;
            movie.update();
        }
        cache.set("userTitles", titleToId);
        cache.set("newMovie", movie);
    }

    private void updateCache(Movie movie) {
        cache.set("userRatingCount", (int) cache.get("userRatingCount") - 1);
        cache.set("userRatingTotal", (double) cache.get("userRatingTotal") - movie.userRating);
        if (movie.rtRating > -1d) {
            cache.set("rtRatingCount", (int) cache.get("rtRatingCount") - 1);
            cache.set("rtRatingTotal", (double) cache.get("rtRatingTotal") - movie.rtRating);
        }
        if (movie.userHit)
            cache.set("userHits", (int) cache.get("userHits") - 1);
    }

    private Long getMaxId(Map<String, Long> userTitles) {
        if (userTitles.size() == 0) return 0L;
        return userTitles.values().stream().max(Long::compare).get();
    }

    private Map<String, Long> getTitleToId() {
        if (Movie.find.findRowCount() > 0) {
            String sql = "SELECT id, user_title as title"
                    + " from movie";
            RawSql rawSql = RawSqlBuilder.parse(sql).create();
            return Ebean.find(IdAndTitle.class)
                    .setRawSql(rawSql)
                    .findList()
                    .stream()
                    .collect(Collectors.toMap(m -> m.title, m -> m.id));
        }
        return new HashMap<>();
    }
}
