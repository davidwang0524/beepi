package models;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;

@Entity
@Sql
public class IdAndTitle {

    public Long id;

    public String title;
}
