package models;

import com.avaje.ebean.Model;
import play.data.format.Formats;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Movie extends Model {
    @Id
    public Long id;

    @Column(unique = true)
    public String userTitle;

    public double userRating;

    public String userSynopsis;

    public boolean userHit;

    public String rtTitle;

    public double rtRating;

    public String rtSynopsis;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    public Date date = new Date();

    public Movie(UserData userData, RottenTomatoData rtData, String newUserRating) {
        if (rtData != null) {
            this.rtRating = rtData.rating;
            this.rtSynopsis = rtData.synopsis.substring(0, rtData.synopsis.length() > 255 ? 255 : rtData.synopsis.length());
            this.rtTitle = rtData.title;
        } else {
            this.rtRating = -1d;
        }
        try {
            this.userRating = Double.parseDouble(newUserRating);
        } catch (Exception e) {
            this.userRating = userData.rating;
        }
        this.userTitle = userData.title;
        this.userSynopsis = userData.synopsis;
        this.userHit = userData.hit;
        this.date = new Date();
    }

    public static Finder<Long, Movie> find = new Finder<>(Movie.class);
}
