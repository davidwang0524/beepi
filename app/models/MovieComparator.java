package models;

import java.util.Comparator;

public class MovieComparator {

    public static Comparator<Movie> BottomFive = (movie1, movie2) -> {
        if (movie1.userRating == movie2.userRating)
            return -1 * movie1.userTitle.compareTo(movie2.userTitle);
        return movie1.userRating < movie2.userRating ? -1 : 1;
    };

    public static Comparator<Movie> TopFive = (movie1, movie2) -> {
        if (movie1.userRating == movie2.userRating)
            return movie1.userTitle.compareTo(movie2.userTitle);
        return movie1.userRating > movie2.userRating ? -1 : 1;
    };
}
