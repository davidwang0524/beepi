package models;

public class RottenTomatoData {

    public String title;

    public double rating;

    public String synopsis;

    public RottenTomatoData(String title, double rating, String synopsis) {
        this.title = title;
        this.rating = rating;
        this.synopsis = synopsis;
    }
}

