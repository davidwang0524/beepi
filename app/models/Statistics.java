package models;

import com.avaje.ebean.annotation.Sql;

import javax.persistence.Entity;

@Entity
@Sql
public class Statistics {

    public int rtRatingCount;

    public double rtRatingAvg;

    public double rtRatingTotal;

    public int userRatingCount;

    public double userRatingAvg;

    public double userRatingTotal;

    public double hitRate;

    public int userHits;

    public Statistics(int userRatingCount, double userRatingTotal, int rtRatingCount, double rtRatingTotal, int userHits) {
        this.rtRatingCount = rtRatingCount;
        this.rtRatingAvg = rtRatingCount == 0 ? 0d : rtRatingTotal / (double) rtRatingCount;
        this.userRatingCount = userRatingCount;
        this.userRatingAvg = userRatingCount == 0 ? 0d : userRatingTotal / (double) userRatingCount;
        this.hitRate = rtRatingCount == 0 ? 0d : (double) userHits / (double) rtRatingCount;
    }
}
