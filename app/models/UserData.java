package models;

import play.data.validation.Constraints;

public class UserData {
    public String synopsis;

    @Constraints.Required
    public String title;

    @Constraints.Required
    public double rating;

    public boolean hit;

    public String validate(){
        if(rating < 1d || rating > 10d)
            return "Rating must be between 1 and 10";
        return null;
    }
}
