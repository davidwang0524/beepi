# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table movie (
  id                        bigint auto_increment not null,
  user_title                varchar(255),
  user_rating               double,
  user_synopsis             varchar(255),
  user_hit                  tinyint(1) default 0,
  rt_title                  varchar(255),
  rt_rating                 double,
  rt_synopsis               varchar(255),
  date                      datetime(6),
  constraint uq_movie_user_title unique (user_title),
  constraint pk_movie primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table movie;

SET FOREIGN_KEY_CHECKS=1;

